package main

import (
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"log"
	"net/http"
	"flag"
	"gopkg.in/yaml.v3"
	"os"
)

var dataStorage = make(map[string][]byte)

type Config struct {
	Server struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
	}
}

func genID(w http.ResponseWriter, _ *http.Request) {
	token := ""
	for {
		token = uuid.New().String()
		if len(dataStorage[token]) == 0 {
			break
		}
	}
	fmt.Fprint(w, token)
}

func saveData(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		token := r.FormValue("token")
		if r.ContentLength > 0 && token != "" {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				panic(err)
			}
			if len(body) == 0 {
				return
			}
			dataStorage[token] = body
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func getData(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("token")
	if token != "" {
		fmt.Fprint(w, string(dataStorage[token]))
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
}

func config() (*Config) {
	cfgEnv := flag.String("config", "etc/developer.yml", "select config name")
	flag.Parse()

	f, err := os.Open(*cfgEnv)
	if err != nil {
		panic(err)
	}

	cfg := &Config{}
	err = yaml.NewDecoder(f).Decode(cfg)
	f.Close()
	if err != nil {
		panic(err)
	}
	return cfg
}

func main() {
	mux := http.NewServeMux()

	mux.HandleFunc("/get_token", genID)
	mux.HandleFunc("/save", saveData)
	mux.HandleFunc("/get", getData)

	cfg := config()

	srv := &http.Server{
		Addr:    fmt.Sprintf("%v:%v", cfg.Server.Host, cfg.Server.Port),
		Handler: mux,
	}

	fmt.Println(cfg.Server.Host)
	fmt.Println(cfg.Server.Port)

	log.Fatal(srv.ListenAndServe())
}
