module gist

go 1.13

require (
	github.com/google/uuid v1.1.1
	gopkg.in/yaml.v3 v3.0.0-20200121175148-a6ecf24a6d71
)
